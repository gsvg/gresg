/* resources.vala
 *
 * Copyright (C) 2017 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GXml;

public errordomain Gresg.ResourcesError {
  INVALID_FILE,
  NO_RESOURCES
}

public class Gresg.Resources : GXml.Element {
  private Resource.Map _map;
  public Resource.Map map {
    get {
      if (_map == null)
          set_instance_property ("map");
      return _map;
    }
    set {
      if (_map != null)
        try { clean_property_elements ("map"); }
        catch (GLib.Error e) { warning ("Error: "+e.message); }
      _map = value;
    }
  }
  private Resource _resource;
  public Resource resource {
    get {
      if (_resource == null)
          set_instance_property ("resource");
      return _resource;
    }
    set {
      if (_resource != null) {
        _resource = map.create_item () as Resource;
        try { map.append (_resource); }
        catch (GLib.Error e ) {
          warning ("Error: "+e.message);
        }
      }
      _resource = value;
    }
  }
  construct {
    try {
      initialize ("gresources");
    }
    catch (GLib.Error e ) {
      warning ("Error: "+e.message);
    }
  }
  public Resources.with_prefix (string prefix) {
    resource.prefix = prefix;
  }
  public void add_file (string filename) throws GLib.Error {
    var f = GLib.Object.new (typeof (File), "owner_document", owner_document) as Gresg.File;
    f.set_name (filename);
    resource.append_child (f);
  }
}

public class Gresg.Resource : GXml.Element, MappeableElement {
  private string _prefixr = "/org/gnome";
  [Description(nick="::prefix")]
  public new string prefix {
    get { return _prefixr; }
    set { _prefixr = value; }
  }
  private File.List _files;
  public File.List files {
    get {
      if (_files == null)
        set_instance_property ("files");
      return _files;
    }
    set {
      if (_files != null)
        try { clean_property_elements ("files"); }
        catch (GLib.Error e) { warning ("Error: "+e.message); }
      _files = value;
    }
  }
  construct {
    try { initialize ("gresource"); }
    catch (GLib.Error e ) {
      warning ("Error: "+e.message);
    }
  }
  public string get_map_key () { return prefix; }
  public class Map : GXml.HashMap {
    construct {
      try { initialize (typeof (Resource)); }
      catch (GLib.Error e ) {
        warning ("Error: "+e.message);
      }
    }
  }
}

public class Gresg.File : GXml.Element {
  construct {
    try { initialize ("file"); }
    catch (GLib.Error e ) {
      warning ("Error: "+e.message);
    }
  }
  public File.with_name (string name) {
    try {
      var t = owner_document.create_text_node (name);
      append_child (t);
    } catch (GLib.Error e) {
      warning ("Error creating a new File node: "+e.message);
    }
  }
  public string get_name () throws GLib.Error {
    if (child_nodes.length == 0)
      throw new ResourcesError.INVALID_FILE ("File's name is invalid");
    for (int i = 0; i < child_nodes.length; i++) {
      var tn = child_nodes.item (i) as DomText;
      if (tn == null) continue;
      return tn.data;
    }
    return "";
  }
  public void set_name (string name) throws GLib.Error {
    for (int i = 0; i < child_nodes.length; i++) {
      var n = child_nodes.item (i) as DomChildNode;
      if (n == null) continue;
      n.remove ();
    }
    var t = owner_document.create_text_node (name);
    append_child (t);
  }
  public class List : GXml.ArrayList {
    construct {
      try { initialize (typeof (File)); }
      catch (GLib.Error e ) {
        warning ("Error: "+e.message);
      }
    }
  }

}
