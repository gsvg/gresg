GIR_NAME= VERSIONED_CAMEL_CASE_NAME+'.gir'
TYPELIB_NAME= VERSIONED_CAMEL_CASE_NAME+'.typelib'
VAPI_NAME = VERSIONED_PROJECT_NAME+'.vapi'

gresg_deps = configure_file(input: 'gresg.deps',
	output: VERSIONED_PROJECT_NAME+'.deps',
	copy: true
	)
install_data(gresg_deps,
	install_dir: vapidir
	)

confh = configuration_data ()
confh.set_quoted('PACKAGE_LOCALE_DIR', join_paths(get_option('prefix'), get_option('datadir'), 'locale'))
confh.set_quoted('GETTEXT_PACKAGE', 'gresg')
confh.set_quoted('PACKAGE_NAME', PROJECT_NAME)
confh.set_quoted('VERSION', PROJECT_VERSION)
configure_file(output : 'config.h',
	configuration : confh)
configvapi = files(['config.vapi'])

gresg_sources = files ([
	'main.vala',
])

libgresg_sources = files ([
	'resources.vala'
])
conf = configuration_data()
conf.set('CAMEL_CASE_NAME', CAMEL_CASE_NAME)
conf.set('PROJECT_NAME', PROJECT_NAME)
conf.set('PROJECT_VERSION', PROJECT_VERSION)
conf.set('API_VERSION', API_VERSION)
nsinfo = configure_file(input : 'namespace-info.vala.in',
	output : 'namespace-info.vala',
	configuration : conf)
namespaceinfo_dep = declare_dependency (sources : nsinfo)

inc_libh = include_directories ('.')
inc_libh_dep = declare_dependency (include_directories : inc_libh)

# LT_VERSION for ABI related changes
# From: https://autotools.io/libtool/version.html
# This rules applies to Meson 0.43
# Increase the current value whenever an interface has been added, removed or changed.
# Always increase revision value whenever an interface has been added, removed or changed.
# Increase the age value only if the changes made to the ABI are backward compatible.
# Set version to the value of subtract age from current
# Reset current and version to 1 and, age and version to 0 if library's name is changed
LT_CURRENT='2'
LT_REVISION='1'
LT_AGE='1'
LT_VERSION='1'
libgresg = library(VERSIONED_PROJECT_NAME,
	libgresg_sources,
	version : LT_VERSION,
	soversion : LT_VERSION+'.'+LT_AGE+'.'+LT_REVISION,
	vala_header : PROJECT_NAME+'.h',
	vala_vapi : VAPI_NAME,
	vala_gir : GIR_NAME,
	dependencies : [ gee, gio, xml, gxml, namespaceinfo_dep, inc_libh_dep, inc_rooth_dep ],
	vala_args: [
		'--abi-stability'
	],
	c_args : [
		'-include',
		meson.current_build_dir() + '/config.h'
	],
	install : true,
	install_dir : [
		true,
		join_paths (get_option('includedir')),
		vapidir,
		true
	])

libgresg_dep = declare_dependency(include_directories : inc_libh, link_with: libgresg)

gresg_deps = [ gee, gio, xml, gxml, namespaceinfo_dep, inc_libh_dep, inc_rooth_dep ]


gresg = executable(PROJECT_NAME,
	gresg_sources + configvapi,
	dependencies : gresg_deps,
	link_with: libgresg,
	install : true,
	install_dir : [
		get_option('bindir')
	])
